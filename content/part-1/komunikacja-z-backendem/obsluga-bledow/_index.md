---
title: "Obsługa błędów"
date: 2019-01-29T21:23:51+01:00
draft: false
weight: 20
---
### Importy: 
```javascript
import { MatSnackBar } from '@angular/material/snackbar';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
```

### Instrukcje:
1. W serwisie `HttpService` (`src/app/core/providers/http.service.ts`) wstrzyknij w konstruktorze `MatSnackBar`
```javascript
export class HttpService {
    constructor(
        private httpClient: HttpClient,
        private matSnackBar: MatSnackBar,
    ) {}
```

2. Stwórz metodę `handleError`, która przyjmie parametr `error` typu `HttpErrorResponse`. Metoda powinna otworzyć snackBar oraz zwrócić błąd.
```javascript
    handleError(error: HttpErrorResponse) {
        this.matSnackBar.open('Wystąpił błąd!', 'Close', {duration: 3000});

        return throwError(`Wystąpił błąd - ${error.status} - ${error.message}`);
    }
```

3. Użyj metody `handleError` za pomocą operatora rxjs `catchError` w metodzie `post$`
```javascript
    post$(url: string, body: any, options?: any): Observable<any> {
        return this.httpClient.post(`${environment.url}${url}`, body, options)
            .pipe(catchError(this.handleError.bind(this))); // <- tu
    }
```
