---
title: "Serwisy"
date: 2019-01-29T21:13:55+01:00
draft: false
weight: 10
---
### Importy: 
```javascript
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment.prod';
import { HttpService } from '../../core/providers/http.service';
import { IQuiz } from '../../shared/interfaces/quiz/quiz.interface';
import { MatSnackBarModule } from '@angular/material/snackbar';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snackbar';
import { QuizService } from '../quiz.service';
import { RoutesConstants } from '../../../shared/constants/routes.constants';
```

### Instrukcje:
1. Zaimpotuj `HttpClientModule` w `AppModule`
```javascript
    @NgModule({
        imports: [
            AppRoutingModule,
            BrowserModule,
            BrowserAnimationsModule,
            SharedModule,
            QuizModule,
            HttpClientModule, // <- tu
        ],
        declarations: [
            AppComponent,
            HeaderComponent,
            ContentComponent,
        ],
        bootstrap: [AppComponent],
    })
    export class AppModule {
        constructor() {
            moment.locale('pl');
        }
    }
```

2. W `SharedModule` (`src/app/shared/shared.module.ts`) dodaj moduł `MatSnackBarModule` do sekcji `imports` i `exports`
```javascript
    @NgModule({
        imports: [
            [...]
            MatInputModule,
            MatSnackBarModule, // <- tu
        ],
        [...]
        exports: [
            [...]
            ToggleControlComponent,
            MatSnackBarModule, // <- tu
        ],
    })
    export class SharedModule {
    }
```

3. W katalogu `src/app/core` dodaj nowy folder `providers` i wewnątrz niego stwórz serwis `HttpService`
```bash
    $ ng g s core/providers/http --lintFix=true --skipTests=true
```

4. W konstruktorze nowo utworzonego serwisu wstrzyknij `HttpClient`
```javascript
    constructor(private httpClient: HttpClient) {
    }
```

5. Dodaj metodę `post$`, która przyjmie trzy parametry: `url` o typie `string`, `body` o typie `any`, opcjonalny parametr `options` typu `any`. Metoda powinna zwracać typ `Observable<any>`.
```javascript
    post$(url: string, body: any, options?: any): Observable<any> {
        return this.httpClient.post(`${environment.url}${url}`, body, options);
    }
```

6. W katalogu `src/app/components/quiz` wygeneruj serwis `QuizService`
```bash
    $ ng g s components/quiz/quiz --lintFix=true --skipTests=true
```

7. Wstrzyknij `HttpService` w konstruktorze `QuizService`
```javascript
    constructor(private httpService: HttpService) { }
```

8. Dodaj metodę `saveQuiz` z parametrem `quiz` typu `IQuiz`. Metoda powinna zwracać typ `Observable<any>`. Wewnątrz powinna odwoływać się do metody `post$` serwisu `HttpService`.
```javascript
    saveQuiz$(quiz: IQuiz): Observable<any> {
        return this.httpService.post$('quizzes', quiz);
    }
```

9. Do konstruktora `QuizAppComponent` (`src/app/components/quiz/quiz-add/quiz-add.component.ts`) wstrzyknij `QuizService`, `Router` i `MatSnackBar`.
```javascript
    constructor(
        private formBuilder: FormBuilder,
        private quizService: QuizService,
        private router: Router,
        private matSnackBar: MatSnackBar,
    ) {
        [...]
    }
```

10. Stwórz metodę `submitQuiz`, która sprawdzi czy formularz zawiera błędy. Jeśli tak - wyświetli SnackBar z informacją. Jeśli nie - wyśle do backendu dane formularza za pomocą metody `saveQuiz` z serwisu `QuizService`. Po udanym zapisie danych metoda powinna przekierować użytkownika na listę quizów.
```javascript
    submitQuiz() {
        if(this.quizForm.valid) {
            this.quizService.saveQuiz$(this.quizForm.value).subscribe(() =>
                this.router.navigateByUrl(RoutesConstants.quiz.getUrl(RoutesConstants.quiz.LIST)),
            );

            return;
        }

        this.matSnackBar.open('Formularz zawiera błędy!', 'Close', {duration: 3000});
    }
```

10. W templatce usuń blokowanie przycisku "Zapisz quiz" i podepnij metodę `submitQuiz` na `click`.
```html
    <button mat-raised-button
            [disabled]="!quizForm.valid"
            color="primary"
            (click)="submitQuiz()"
            class="mt-25 btn-submit">Zapisz quiz
    </button>
```
