---
title: "Komunikacja z backendem"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 40
chapter: true
pre: "<b>4. </b>"
---

### Rozdział 4

# Komunikacja z backendem

W tym rozdziale skomunikujesz swój frontend z backendem.

### Git branch:

```bash
$ git checkout part1/komunikacja-z-backendem
```
