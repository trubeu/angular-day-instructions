---
title: "Podpięcie formularza pod templatkę"
date: 2019-01-29T18:33:01+01:00
weight: 30
draft: false
---

Do templatki komponentu `QuizAddComponent` (`src/app/components/quiz/quiz-add/quiz-add.component.html`) skopiuj poniższy kod:

```html
<div class="QuizAdd">
  <form [formGroup]="quizForm">
    <mat-card>
      <div>
        <mat-form-field>
          <input matInput
                 formControlName="title"
                 placeholder="Tytuł quizu"
                 class="text-large">
        </mat-form-field>
      </div>

      <mat-form-field>
        <input matInput
               formControlName="description"
               placeholder="Krótki opis quizu">
      </mat-form-field>
    </mat-card>
    <mat-accordion>
      <mat-expansion-panel #expansionPanel
                           (opened)="activeQuestionIndex = questionIndex"
                           [expanded]="activeQuestionIndex === questionIndex"
                           *ngFor="let questionFormGroup of quizForm.get('questions')?.controls || []; let questionIndex = index"
                           [formGroup]="questionFormGroup">
        <mat-expansion-panel-header *ngIf="!expansionPanel.expanded">
          <mat-panel-title>Pytanie {{ questionIndex + 1}}</mat-panel-title>
          <mat-panel-description>Odpowiedzi: {{ questionFormGroup.get('answers').controls.length }}</mat-panel-description>
        </mat-expansion-panel-header>

        <mat-form-field [class.mt-25]="expansionPanel.expanded">
          <input matInput
                 formControlName="question"
                 [placeholder]="'Pytanie ' + (questionIndex + 1)"
                 class="QuizAdd__question">
        </mat-form-field>

        <mat-icon matTooltip="Usuń pytanie"
                  class="ml-25 QuizAdd__remove-question">delete
        </mat-icon>

        <span class="ml-25">Tryb odpowiedzi</span>
        <mat-slide-toggle color="primary"
                          formControlName="singleAnswerMode"
                          (click)="deselectAllAnswers(questionFormGroup)"
                          class="ml-25"></mat-slide-toggle>

        <mat-list class="mb-25">
          <mat-list-item *ngFor="let answerFormGroup of questionFormGroup.get('answers').controls; let answerIndex = index"
                         [formGroup]="answerFormGroup">
            <div mat-line>
              <mat-form-field>
                <input matInput
                       formControlName="answer"
                       [placeholder]="'Odpowiedź ' + (answerIndex + 1)">
              </mat-form-field>
              <mat-checkbox color="primary"
                            formControlName="correct"
                            (change)="markAnswerAsCorrect(questionFormGroup, answerIndex)"
                            class="ml-25">Poprawna odpowiedź</mat-checkbox>
              <mat-icon matTooltip="Usuń odpowiedź"
                        class="ml-25 QuizAdd__remove-answer">
                delete
              </mat-icon>
            </div>
          </mat-list-item>
        </mat-list>
        <a class="link">Dodaj odpowiedź</a>
      </mat-expansion-panel>
    </mat-accordion>

    <button mat-raised-button
            color="primary"
            class="mt-25 btn-submit">Zapisz quiz
    </button>

    <button mat-stroked-button
            color="primary"
            type="button"
            class="mt-25 ml-15">Dodaj pytanie
    </button>
  </form>
</div>
```
