---
title: "Import modułów"
date: 2019-01-29T17:24:29+01:00
weight: 10
draft: false
---

### Importy:
```javascript
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
```

### Instrukcje:

W `QuizModule` (`src/app/components/quiz/quiz.module.ts`) zaimportuj `FormsModule`, `ReactiveFormsModule`.

```javascript
@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MatTableModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatExpansionModule,
    FormsModule, // <- tu 
    ReactiveFormsModule, // <- i tu
  ],
  declarations: [
    QuizComponent,
    QuizListComponent,
    QuizAddComponent,
    QuizSolveComponent,
  ]
})
export class QuizModule {
}
```
