---
title: "Tworzenie formularza"
date: 2019-01-29T17:29:07+01:00
draft: false
weight: 20
---

### Importy:
```javascript
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
```

### Instrukcje:

W komponencie `QuizAddComponent` (`src/app/components/quiz/quiz-add/quiz-add.component.ts`):

1. Do klasy dodaj pole `activeQuizIndex` o typie `number` i zainicjalizuj je wartością `0` oraz pole `quizForm` o typie `FormGroup` bez wartości początkowej.
```javascript
    activeQuestionIndex: number = 0;
    quizForm: FormGroup;
```

2.  W konstruktorze komponentu wstrzyknij klasę `FormBuilder`, a następnie użyj jej do zainicjalizowania formularza. Formularz powinien zawierać trzy pola: `title` (z walidatorem required), `description`, `questions`. Pole `questions` powinno być tablicą pól.
```javascript
    constructor(private formBuilder: FormBuilder) {
        this.quizForm = this.formBuilder.group({
            title: ['', [Validators.required]],
            description: '',
            questions: this.formBuilder.array([]),
        });
    }
```

3.  Stwórz metodę `getQuestionFormGroup`, która będzie zwracała grupę formularza. Zwracana grupa powinna zawierać pola `question` (z walidatorem required), `singleAnswerMode` oraz tablicę pól `answers`.
```javascript
    getQuestionFormGroup(): FormGroup {
        return this.formBuilder.group({
            question: ['', [Validators.required]],
            singleAnswerMode: true,
            answers: this.formBuilder.array([]),
        });
    }
```

4.  Stwórz metodę `getAnswerFormGroup`, która będzie zwracała grupę formularza z polami `answer` i `correct`.
```javascript
    getAnswerFormGroup(): FormGroup {
        return this.formBuilder.group({
            answer: '',
            correct: false,
        });
    }
```

5.  Użyj metody `getQuestionFormGroup` wewnątrz deklaracji tablicy pól pola `questions` w konstruktorze komponentu.
```javascript
    constructor(private formBuilder: FormBuilder) {
        this.quizForm = this.formBuilder.group({
            title: ['', [Validators.required]],
            description: '',
            questions: this.formBuilder.array([
                this.getQuestionFormGroup(), // <- tu
            ]),
        });
    }
```


6.  Użyj dwukrotnie metody `getAnswerFormGroup` wewnątrz deklaracji tablicy pól w polu `answers` w metodzie `getQuestionFormGroup`.
```javascript
    getQuestionFormGroup(): FormGroup {
        return this.formBuilder.group({
            question: ['', [Validators.required]],
            singleAnswerMode: true,
            answers: this.formBuilder.array([
                this.getAnswerFormGroup(), // <- tu
                this.getAnswerFormGroup(), // <- i tu
            ]),
        });
    }
```

7. Dodaj metodę `deselectAllAnswers`, która na przekazanym Form groupie usunie wszystkie oznaczenia prawidłowych odpowiedzi.
```javascript
    deselectAllAnswers(questionFormGroup: FormGroup) {
        const answers = (questionFormGroup.get('answers') as FormArray);
        const newAnswers = [...answers.value];

        newAnswers.map((answer) => answer.correct = false);
        answers.patchValue(newAnswers, {emitEvent: false});
    }
```

8. Na sam koniec dodaj jeszcze metodę `markAnswerAsCorrect`, która przyjmie `quesionFormGroup` oraz `answerIndex`. Metoda ta, w zależności od wybranego trybu liczby poprawnych odpowiedzi, powinna umożliwiać wybranie tylko jednej lub kilku odpowiedzi. Do czyszczenia poprawnych odpowiedzi wykorzystaj metodę `deselectAllAnswers`
```javascript
    markAnswerAsCorrect(questionFormGroup: FormGroup, index: number) {
        if(!questionFormGroup.get('singleAnswerMode').value) {
            this.deselectAllAnswers(questionFormGroup);
            (questionFormGroup.get('answers') as FormArray).at(index).patchValue({correct: true});
        }
    }
```
