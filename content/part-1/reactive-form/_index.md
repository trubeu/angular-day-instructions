---
title: "Reactive Form"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
chapter: true
pre: "<b>1. </b>"
---

### Rozdział 1.
Podstawa całej aplikacji - formularz. Dowiesz się jak budować reaktywne formularze w oparciu o FormBuildera. 

Wymagania:

* Formularz powinien przyjmować tytuł quizu oraz jego krótki opis.
* Tytuł quizu jest wymagany.
* Można dodawać nieskończoną ilość pytań.
* Do każdego pytania można dodać nieskończoną ilość odpowiedzi.
* Każde pytanie musi zawierać mimimum dwie odpowiedzi.



### Git branch:
```bash
$ git checkout part1/reactive-forms
```

