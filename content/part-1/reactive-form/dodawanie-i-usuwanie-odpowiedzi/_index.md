---
title: "Dodawanie i usuwanie odpowiedzi"
date: 2019-01-29T18:46:19+01:00
draft: false
weight: 50
---

W komponencie `QuizAddComponent` (`src/app/components/quiz/quiz-add/quiz-add.component.ts`):

1. Dodaj metodę `addNewAnswer`, która będzie przyjmować parametr `formGroup` o typie `FormGroup`. 
Metoda powinna dodać do tablicy pól `answers` tego obiektu kolejną grupę formularza (użyj metody `getAnswerFormGroup` klasy komponentu).

```javascript
    addNewAnswer(formGroup: FormGroup) {
        (formGroup.get('answers') as FormArray).push(this.getAnswerFormGroup());
    }
```

2. Dodaj metodę `removeAnswer`. Metoda powinna przyjmować dwa parametry: `formGroup` o typie `FormGroup` (referencja do grupy formularza pytania z którego odpowiedź należy usunąć), `answerIndex` o typie `number` (określa index pytania do usunięcia).
 Przy użyciu metody `removeAt` usuń odpowiedni element z tablicy pól w przekazanej grupie formularza. Uwzględnij logikę weryfikującą, czy każde pytanie zawiera minimum dwie odpowiedzi.
```javascript
    removeAnswer(formGroup: FormGroup, answerIndex: number) {
        const answersFormGroup: FormArray = (formGroup.get('answers') as FormArray);
        const minAnswersCount: number = 2;

        if(answersFormGroup.length > minAnswersCount) {
            answersFormGroup.removeAt(answerIndex);
        }
    }
```

3. Dodaj metody do odpowiednich eventów w templatce.
```html
    <mat-icon matTooltip="Usuń odpowiedź"
              class="ml-25 QuizAdd__remove-answer"
              (click)="removeAnswer(questionFormGroup, answerIndex)">
        delete
    </mat-icon>
[…]
    <a class="link"
       (click)="addNewAnswer(questionFormGroup)">
        Dodaj odpowiedź
    </a>
```
