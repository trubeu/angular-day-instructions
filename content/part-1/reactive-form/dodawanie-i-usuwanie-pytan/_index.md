---
title: "Dodawanie i usuwanie pytań"
date: 2019-01-29T18:41:34+01:00
draft: false
weight: 40
---

### Importy: 
```javascript
import { FormArray } from '@angular/forms';
```

### Instrukcje:
W komponencie `QuizAddComponent` (`src/app/components/quiz/quiz-add/quiz-add.component.ts`):

1. Dodaj metodę `addNewQuestion`, która do istniejącego pola `questions` w formularzu `quizForm` doda kolejną grupę formularza zwracaną przez metodę `getQuestionFormGroup`. Dodatkowo metoda powinna ustawić index aktywnego pytania na nastepne:
```javascript
    addNewQuestion() {
        (this.quizForm.get('questions') as FormArray).push(this.getQuestionFormGroup());
        this.activeQuestionIndex++;
    }
```

2. Dodaj metodę `removeQuestion`, która przyjmie parametr `questionIndex` o typie `number` i przy użyciu metody `removeAt` usunie konkretną grupę z tablicy pól `questions` z formularza `quizForm`. Po usunięciu powinno zostać zaznaczone poprzednie pytanie.
```javascript
    removeQuestion(questionIndex: number) {
        (this.quizForm.get('questions') as FormArray).removeAt(questionIndex);
        this.activeQuestionIndex = questionIndex - 1;
    }
```

3. Obie metody podepnij do formularza na eventy `click`:
```html
    […]
    <mat-icon matTooltip="Usuń pytanie"
            (click)="removeQuestion(questionIndex)"
            class="ml-25 QuizAdd__remove-question">delete
    </mat-icon>
    […]
    <button mat-stroked-button
            color="primary"
            type="button"
            (click)="addNewQuestion()"
            class="mt-25 ml-15">Dodaj pytanie
    </button>
    […]
```

