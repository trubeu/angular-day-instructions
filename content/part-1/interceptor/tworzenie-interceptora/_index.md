---
title: "Tworzenie interceptora"
date: 2019-01-30T15:57:04+01:00
draft: false
weight: 10
---
### Importy:
```javascript
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './providers/error.interceptor';
import { CoreModule } from './core/core.module';
```

### Instrukcje:
0. W pliku `src\environments\envioronemnt.ts` zamień wartość klucza url na:
```
https://l7fgim0za7.execute-api.us-east-1.amazonaws.com/prod/
```

1. W katalogu `src/app/core/providers` stwórz nowy plik o nazwie `error.interceptor.ts`. Niestety CLI nie ma schematicsa dla interceptorów, więc musisz to zrobić ręcznie :(.

2. Wewnątrz stwórz klasę `ErrorInterceptor`, która implementuje interfejs `HttpInterceptor`. Wewnątrz klasy dodaj metodę `intercept`, która przyjmie dwa parametry: `request` (typu`HttpRequest<any>`), `next` (typu `HttpHandler`). Metoda ta powinna zwracać typ `Observable<HttpEvent<any>>`.
```javascript
  @Injectable()
    export class ErrorInterceptor implements HttpInterceptor {
        intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
            return next.handle(request);
        }
    }
```

3. Za pomocą CLI utwórz moduł `CoreModule`.
```bash
$ ng g m core/core --flat --lintFix=true
```

3. W sekcji `providers` modułu `CoreModule` użyj providera `HTTP_INTERCEPTORS` żeby dodać interceptor.
```javascript
    @NgModule({
        providers: [
            {
                provide: HTTP_INTERCEPTORS,
                useClass: ErrorInterceptor,
                multi: true,
            },
        ],
    })
    export class CoreModule { }
```

4. Zaimportuj moduł `CoreModule` w `AppModule`.
```javascript
@NgModule({
    imports: [
        [...]
        HttpClientModule,
        CoreModule,
    ],
    [...]
})
```
