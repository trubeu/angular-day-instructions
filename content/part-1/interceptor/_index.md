---
title: "Interceptor"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 50
chapter: true
pre: "<b>5. </b>"
---

### Rozdział 5

# Interceptor

Pora zamienić brzydką obsługę błędów na ładną - w interceptorze.

### Git branch:

```bash
$ git checkout part1/interceptor
```
