---
title: "Dodanie wyświetlania błędu"
date: 2019-01-30T16:16:42+01:00
draft: false
weight: 20
---
### Importy: 
```javascript
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snackbar';
```

### Instrukcje:
1. W `CoreModule` dodaj provider dla `MatSnackBar`. Dodaj go również w polu `deps` providera interceptora.
```javascript
@NgModule({
  providers: [
      MatSnackBar,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor,
        deps: [MatSnackBar],
        multi: true
      }
  ]
})
export class CoreModule { }
```

1. Do konstruktora interceptora `ErrorInterceptor` wstrzyknij `MatSnackBar`.
```javascript
    constructor(private matSnackBar: MatSnackBar) {
    }
```

2. W interceptorze stwórz metodę `handleError`, która przyjmie parametr `error` typu `HttpErrorResponse`. Będzie zwracała typ `Observable<never>`. W metodzie tej dodaj wyświetlanie snackbara z informacją o błędzie.
```javascript
    handleError$(error: HttpErrorResponse): Observable<never> {
        this.matSnackBar.open('Wystąpił błąd!', 'Close', {duration: 3000});

        return throwError(`Wystąpił błąd - ${error.status} - ${error.message}`);
    }
```

3. Użyj metody jako parametr operatora `catchError` w pipelinie w metodzie `intercept`. Pamiętaj o bindowaniu kontekstu.
```javascript
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
            .pipe(catchError(this.handleError$.bind(this)));
    }
```

4. Na koniec usuń wyświetlanie błędu z serwisu `HttpService`.
    * Usuń metodę `handleError`.
    * Usuń cały `pipe` razem z `cacthError`.
    * Usuń wstrzyknięcie `MatSnackBar` z konstruktora serwisu.
    * Nie zapomnij o nieużywanych importach!
    * Twój kod powinien wyglądać tak:
```javascript
    export class HttpService {
        constructor(private httpClient: HttpClient) {
        }
        
        post$(url: string, body: any, options?: any): Observable<any> {
            return this.httpClient.post(`${environment.url}${url}`, body, options);
        }
    }
```
