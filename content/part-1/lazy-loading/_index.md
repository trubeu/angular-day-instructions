---
title: "Lazy loading"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 60
chapter: true
pre: "<b>6. </b>"
---

### Rozdział 5

# Lazy loading

W tej części dowiesz się w jaki sposób leniwie ładować moduły.

### Git branch:

```bash
$ git checkout part1/lazy-loading
```