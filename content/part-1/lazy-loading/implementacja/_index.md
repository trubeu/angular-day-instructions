---
title: "Implementacja"
date: 2019-01-30T17:14:09+01:00
draft: false
weight: 10
---

### Importy: 
```javascript
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConstants } from '../../shared/constants/routes.constants';
import { QuizAddComponent } from './quiz-add/quiz-add.component';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizSolveComponent } from './quiz-solve/quiz-solve.component';
import { QuizComponent } from './quiz.component';
import { QuizRoutingModule } from './quiz-routing.module';
```

### Instrukcje:

1. Z sekcji `imports` modułu `AppModule` usuń `QuizModule`.

2. W katalogu `./src/app/components/quiz/` obok pliku `quiz.module.ts` stwórz plik `quiz-routing.module.ts`.

3. Wewnątrz pliku stwórz stałą `routes` o typie `Routes` i przenieś tam wszystkie routy z `AppRoutingModule`. Właściwość `path` zmień na pusty string. Następnie do sekcji `imports` dodaj `RoutingModule` ze zdefiniowanymi routami. Pamiętaj też, żeby eksportować `RouterModule`
```javascript
    const routes: Routes = [
        {
            path: '',
            component: QuizComponent,
            children: [
                {
                    path: '',
                    pathMatch: 'full',
                    redirectTo: RoutesConstants.quiz.LIST,
                },
                {
                    path: RoutesConstants.quiz.LIST,
                    component: QuizListComponent,
                },
                {
                    path: RoutesConstants.quiz.ADD,
                    component: QuizAddComponent,
                },
                {
                    path: RoutesConstants.quiz.SOLVE,
                    component: QuizSolveComponent,
                },
            ],
        },
    ];
    @NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [
            RouterModule,
        ],
    })
    export class QuizRoutingModule { }
```

4. Zaimportuj `QuizRoutingModule` w module `QuizModule`.
```javascript
@NgModule({
      imports: [
        [...]
        ReactiveFormsModule,
        QuizRoutingModule, // <- tu
      ],
        [...]
})
export class QuizModule {
}
```


4. W pliku `app-routing.module.ts` w `appRoputes` wskaż na moduł oraz podaj scieżkę dla które moduł ma się załadować:
```javascript
    const appRoutes: Routes = [
        {
            path: RoutesConstants.quiz.BASE_PATH,
            loadChildren: () => import('./components/quiz/quiz.module').then((module) => module.QuizModule),
        },
    ];

    @NgModule({
        imports: [
            RouterModule.forRoot(appRoutes),
        ],
        exports: [RouterModule],
    })
    export class AppRoutingModule {}
```
