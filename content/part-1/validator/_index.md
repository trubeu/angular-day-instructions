---
title: "Validator"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 30
chapter: true
pre: "<b>3. </b>"
---

### Rozdział 3.

# Własny walidator

W tym rozdziale stworzysz swój własny walidator, który będzie sprawdzał, czy przynajmniej jedna prawidłowa odpowiedź jest wybrana. Walidator umieścisz w klasie `QuizAddComponent`.

### Git branch:

```bash
$ git checkout part1/validator
```
