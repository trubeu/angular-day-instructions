---
title: "Tworzenie i implementacja"
date: 2019-01-29T21:06:27+01:00
draft: false
---

### Importy: 
```javascript
import { some } from 'lodash';
import { AbstractControl, ValidationErrors } from '@angular/forms';
```

### Instrukcje:

1. W komponencie `QuizAddComponent` (`./src/app/components/quiz/quiz-add.component.ts`) dodaj metodę `atLeastOneAnswerSelected`. Metoda powinna przyjmować parametr `control` typu `AbstractControl` oraz zwracać typ `ValidationErrors | null`. Powinna sprawdzać, czy w wartości przekazanej referencji do pola jest przynajmnej jedna wartość `true`. Jeśli tak to nie powinna zwracać nic. W przeciwnym przypadku powinna zwracać obiekt `{noCheckedValue: true}`. Pomoże Ci w tym funkcja `some` z lodasha.
```javascript
    atLeastOneAnswerSelected(control: AbstractControl): ValidationErrors | null {
        return some(control.value, (answer) => answer.correct === true) ? null : {noCheckedValue: true};
    }
```

2. Dodaj walidator do pola `answers` w metodzie `getQuestionFormGroup`
```javascript
    getQuestionFormGroup(): FormGroup {
        return this.formBuilder.group({
            question: [''],
            singleAnswerMode: true,
            answers: this.formBuilder.array([
                this.getAnswerFormGroup(),
                this.getAnswerFormGroup(),
            ], this.atLeastOneAnswerSelected), // <- tu
        });
    }
```

3. W templatce dodaj blokowanie przycisku, jeśli formularz zawiera błędy.
```html
    <button mat-raised-button
            [disabled]="!quizForm.valid"
            color="primary"
            class="mt-25 btn-submit">Zapisz quiz
    </button>
```
