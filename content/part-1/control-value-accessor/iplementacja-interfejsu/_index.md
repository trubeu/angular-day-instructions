---
title: "Iplementacja interfejsu i logiki kontrolki"
date: 2019-01-29T19:42:37+01:00
draft: false
weight: 20
---

### Importy: 
```javascript
import { forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
```

1. W komponencie `ToggleControlComponent` (`src/app/shared/components/toggle-control`) zaimplementuj interfejs `ControlValueAccessor`.
```javascript
    export class ToggleControlComponent implements ControlValueAccessor {
```

2. Do klasy komponentu dodaj metody interfejsu oraz dwa własne placeholdery na metody.
```javascript
    onChange = (_) => {}; // <- placeholder
    onTouched = () => {}; // <- placeholder

    registerOnChange() {
    }

    registerOnTouched() {
    }

    writeValue() {
    }
```

3. Użyj tokena `NG_VALUE_ACCESSOR` w sekcji `providers` dekoratora komponentu.
```javascript
    @Component({
        selector: 'app-toggle-control',
        templateUrl: './toggle-control.component.html',
        styleUrls: ['./toggle-control.component.scss'],
        providers: [
            {
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => ToggleControlComponent),
                multi: true,
            },
        ],
    })
```

4. Dodaj logikę do poszczególnych metod:
    * W metodzie `registerOnchange` przypisz przekazaną funkcję do wcześniej zdefiniowanej metody `onChange`.
    ```javascript
        registerOnChange(fn: any) {
            this.onChange = fn;
        }
    ```

    * Tak samo w metodzie `registerOnTouched` przypisz przekazaną funkcję do `onTouched`.
    ```javascript
        registerOnTouched(fn: any) {
            this.onTouched = fn;
        }
    ```

    * W metodzie `writeValue` przypisz przekazaną wartość do właściwości `value` kontrolki.
    ```javascript
        writeValue(value: string) {
            this.value = value;
        }
    ```

    * Dodaj nową metodę `selectOption`, która przyjmie wartość `value`, przypisze ją do wartości kontrolki i wyemituje jej zmianę.
    ```javascript
        selectOption(value: any) {
            this.value = value;
            this.onChange(this.value);
        }
    ```

5. Na sam koniec w templatce dodaj wywołanie metody `selectOption`.
```html
    <span *ngFor="let option of options"
          [ngClass]="{'is-active': value === option.value}"
          (click)="selectOption(option.value)">
      {{ option.label }}
    </span>
```
