---
title: "Iplementacja kontrolki w komponencie"
date: 2019-01-29T20:44:18+01:00
draft: false
weight: 30
---
1. W komponencie `QuizAddComponent` dodaj właściwość `answerModes` typu `IToggleControlValue[]`, która będzie zawierała możliwe do wybrania w kontrolce opcje.
 Nasza kontrolka powinna zwracać wartość `true` jeśli pytanie ma mieć jedną poprawną odpowiedź. W przeciwnym razie powinna zwracać wartość `false`.
```javascript
    answerModes: IToggleControlValue[] = [
        {label: 'pojedynczy', value: true},
        {label: 'wielokrotny', value: false},
    ];
```

2. Dodaj metodę `clearAllCorrectAnswers`, która przyjmie parametr `questionFormGroup` typu `FormGroup` i która wyczyści odpowiedzi oznaczone jako prawidłowe.
```javascript
clearAllCorrectAnswers(questionFormGroup: FormGroup) {
        const answersFormGroup = (questionFormGroup.get('answers') as FormArray);
        const newAnswers = [...answersFormGroup.value];
    
        newAnswers.map((answer) => answer.correct = false);
        answersFormGroup.patchValue(newAnswers, {emitEvent: false});
}
```

3. W templatce komponentu przekaż przed chwilą dodane wartości do kontrolki `app-toggle-control`. Podepnij również metodę `clearAllCorrectAnswers` na `click`.
```html
    <app-toggle-control formControlName="singleAnswerMode"
                        (click)="clearAllCorrectAnswers(questionFormGroup)"
                        [options]="answerModes">
    </app-toggle-control>
```

4. Dodaj metodę `markAnswerAsCorrect`, która przyjmie `quesionFormGroup` oraz `answerIndex`. Metoda ta, w zależności od tego czy tryb pojedynczej odpowiedzi jest wybrany, powinna umożliwiać wybranie tylko jednej lub kilku odpowiedzi. Do czyszczenia poprawnych odpowiedzi wykorzystaj metodę `clearAllCorrectAnswers`
```javascript
    markAnswerAsCorrect(questionFormGroup: FormGroup, index: number) {
        if(questionFormGroup.get('singleAnswerMode').value) {
            this.clearAllCorrectAnswers(questionFormGroup);
            (questionFormGroup.get('answers') as FormArray).at(index).patchValue({correct: true});
        }
    }
```

5. W templatce komponentu dodaj obsługę przed chwilą stworzonej metody (tak aby była wywołana przy oznaczeniu poprawnych odpowiedzi).

Ten kod:
```html
              <mat-checkbox color="primary"
                            formControlName="correct"
                            class="ml-25">Poprawna odpowiedź</mat-checkbox>
```
Zamień na:
```html
              <mat-checkbox color="primary"
                            formControlName="correct"
                            (change)="markAnswerAsCorrect(questionFormGroup, answerIndex)"
                            class="ml-25">Poprawna odpowiedź</mat-checkbox>
```
