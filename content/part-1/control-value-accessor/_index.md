---
title: "Control Value Accessor"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
chapter: true
pre: "<b>2. </b>"
---

### Rozdział 2

W tym rozdziale wykorzystasz interfejs `ControlValueAccessor`, który pozwoli Ci na zintegrowanie Twojego komponentu z API Angular forms.
 Nauczysz się sposobu wykorzystywania tego interfejsu do tworzenia customowych kontrolek, dostarczających najróżniejsze typy danych do Twoich formularzy.

### Git branch:

```bash
$ git checkout part1/control-value-accessor
```
