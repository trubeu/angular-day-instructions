---
title: "Tworzenie komponentu kontrolki"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
---

### Importy: 
```javascript
import { Component, Input } from '@angular/core';
```

1. W katalogu `src/app/shared` utwórz katalog `components`, a w nim komponent `toggle-control` (używając CLI Angulara).
```bash
    $ ng g c shared/components/toggle-control --lintFix=true --skipTests=true
```

2. Nowy komponent powinien z automatu zostać dodany do sekcji `declarations` modułu `SharedModule`. Dodaj go jeszcze do sekcji `exports`.
```javascript
    @NgModule({
        imports: [
            [...]
        ],
        declarations: [
            Pipes.DatePipe,
            ToggleControlComponent, // <- tu dodany jest z automatu
        ],
        exports: [
            [...]
            Pipes.DatePipe,
            ToggleControlComponent, // <- tu
        ]
    })
    export class SharedModule {
    }
```

3. Metoda `ngOnInit` nie będzie wykorzystywana więc możesz ją usunąć (pamiętaj o tym, żeby usunąć również implementację jej interfejsu w klasie oraz import interfejsu).

4. Do klasy komponentu dodaj właściwości `value` oraz `options`. Właściwość `value` będzie trzymała aktualną wartość kontrolki. Właściwość `options` będzie zawierała możliwe opcje wyboru, które będą dostarczane z zewnątrz komponentu.
```javascript
    @Input() options;
    value: any;
```

5. Utwórz plik `toggle-control.interface.ts` w katalogu komponentu `toggle-control`, a nastęnie  stwórz w nim interfejs `IToggleControlValues`. Nowy interfejs przypisz do właściwości options w klasie komponentu.
```javascript
    export interface IToggleControlValue {
        label: string;
        value: any;
    }
```
```javascript
    @Input() options: IToggleControlValue[];
```

6. Do pliku `toggle-control.component.scss` dodaj style
```css
    @import "../../../../assets/styles/variables";

    :host {
        display: inline-block;
        margin-left: 15px;
    }

    span {
        display: inline-block;
        padding: 2px 5px;
        border-radius: 4px;
        margin-right: 5px;
        cursor: pointer;
    }

    .is-active {
        background-color: $text-light-color;
        color: $text-dark-color;
    }
```

7. Do templatki `toggle-control.component.html` dodaj html:
```html
    <span *ngFor="let option of options"
          [ngClass]="{'is-active': value === option.value}">
        {{ option.label }}
    </span>
```

8. Dodaj kontrolkę do templatki komponentu `QuizAddComponent` (`src/app/components/quiz/quiz-add.component.html`):
W miejsce 
```html
    <mat-slide-toggle color="primary"
                      formControlName="singleAnswerMode"
                      (click)="deselectAllAnswers(questionFormGroup)"
                      class="ml-25"></mat-slide-toggle>
```
wstaw:
```html
    <app-toggle-control [options]="[{label: 'lorem', value: 'lorem'}, {label: 'ipsum', value: 'ipsum'}]"
                        (click)="deselectAllAnswers(questionFormGroup)">
    </app-toggle-control>
```
