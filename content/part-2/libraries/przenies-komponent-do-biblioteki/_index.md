---
title: "Przeniesienie komponentu do nowej biblioteki"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
---

### Instrukcje:

1. Przenieś wszystkie pliki z folderu `src/app/shared/components/toggle-control`
do folderu `projects/toggle-control/src/lib`

2. W pliku `projects/toggle-control/src/lib/toggle-control.component.ts` zmień selektor komponentu na `lib-toggle-control`

3. W pliku `toggle-control.components.scss` zmień ścieżkę do zmiennych scss: `@import "../../../../src/assets/styles/variables"`

4. Zbuduj bibliotekę
```bash
    ng build toggle-control
```
