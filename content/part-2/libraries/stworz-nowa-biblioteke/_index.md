---
title: "Tworzenie nowej biblioteki"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
---

### Instrukcje:

1. Stwórz nową bibliotekę, która będzie zawierać w sobie `ToggleControlComponent`
```bash
    $ ng g library toggle-control
```

2. Przejdź do folderu `projects/toggle-control/src/lib` i usuń z niego pliki

    * toggle-control.service.spec.ts
    * toggle-control.service.ts
    * toggle-control.component.spec.ts

3. Edytuj zawartość pliku `projects/toggle-control/src/public_api.ts`
```javascript
    export * from './lib/toggle-control.component';
    export * from './lib/toggle-control.module';
    export * from './lib/toggle-control.interface';
```

4. Edytuj zawartość pliku `projects/toggle-control/src/package.json` (dodaj `@angular/forms` do peerDependencies)
```javascript
    {
        "name": "toggle-control",
        "version": "0.0.1",
        "peerDependencies": {
            "@angular/common": "~9.0.4",
            "@angular/core": "~9.0.4",
            "@angular/forms": "~9.0.4"
        }
    }
```

5. Edytuj zawartość pliku `projects/toggle-control/src/lib/toggle-control.module.ts` (dodaj `CommonModule` do imports)
```javascript
    @NgModule({
      imports: [
        CommonModule,
      ],
      declarations: [ToggleControlComponent],
      exports: [ToggleControlComponent],
    })
    export class ToggleControlModule {}
```
