---
title: "Użycie nowej biblioteki"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 30
---

### Instrukcje:

1. Przejdź do pliku `quiz-add.component.html` i edytuj selektor komponentu `ToggleControl`
```html
    <!-- zamień -->
    <app-toggle-control formControlName="singleAnswerMode"
                (click)="clearAllCorrectAnswers(questionFormGroup)"
                [options]="answerModes">
    </app-toggle-control>
    
    <!-- zamień na -->
    <lib-toggle-control formControlName="singleAnswerMode"
                (click)="clearAllCorrectAnswers(questionFormGroup)"
                [options]="answerModes">
    </lib-toggle-control>
```

2. Przejdź do pliku `src/app/shared/shared.module.ts` zamień import komponentu `ToggleControlComponent` na import modułu `ToggleControlModule`.
Usuń komponent z tablic declarations i exports. W tablicach imports i exports dodaj `ToggleControlModule`.
```javascript
    // zamien
    import { ToggleControlComponent } from '../../../projects/toggle-control/src/lib/toggle-control.component';
    
    // zamien na
    import { ToggleControlModule } from 'toggle-control';
```
<br />
### Gratulacje! Dotarłeś do końca warsztatu!
