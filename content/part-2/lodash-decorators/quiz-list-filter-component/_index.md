---
title: "Quiz list filter component - Memoize, Debounce, Once lodash decorators"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
---

### Instrukcje:

1. W folderze `src/app/components/quiz/quiz-list` stwórz nowy komponent `QuizListFilterComponent`.
```bash
    $ ng g c components/quiz/quiz-list/quiz-list-filter  --lintFix=true --skipTests=true
```

2. W pliku `quiz-list.component.html` dodaj nowostworzony komponent:
```html
    <div class="container-content">
      <app-quiz-list-filter (onFilter)="updateList($event)"
                            [quizzes]="quizzes"></app-quiz-list-filter>
    
      <table *ngIf="quizzes.length; else noQuizzes"
    ...
```

3. W pliku `quiz-list.component.ts` dodaj metodę `updateList`:
```javascript
    updateList(quizzes: IQuiz[]) {
        this.quizzes = quizzes;
    }
```

4. W pliku `quiz-list-filter.component.ts` dodaj atrybut przetrzymujący dane 
dot. quizzów oraz liczby quizzów po filtrowaniu:
```javascript
    resultsAmount: number;
    private quizzes: IQuiz[];
```

5. W pliku `quiz-list-filter.component.ts` dodaj dane wejściowe `@Input`
```javascript
    @Input('quizzes') set setQuizzes(data: IQuiz[]) {
        this.quizzes = data;
    }
```

6. W pliku `quiz-list-filter.component.ts` dodaj `@Output` sygnalizujący zmianę przefiltrowanych quizzów
```javascript
  @Output() readonly onFilter: EventEmitter<IQuiz[]> = new EventEmitter();
```

7. W pliku `quiz-list-filter.component.ts` dodaj logikę filtrującą quizy:
```javascript
    getFilteredQuizzes(text: string): IQuiz[] {
        return this.quizzes.filter((item: IQuiz) => item.title.toLowerCase().startsWith(text.toLowerCase()));
    }
```

8. W pliku `quiz-list-filter.component.ts` dodaj logikę obsługującą event `input` na inpucie w template
```javascript
    onType(text: string) {
        const filteredQuizzes: IQuiz[] = this.getFilteredQuizzes(text);
        
        this.onFilter.emit(filteredQuizzes);
        this.resultsAmount = filteredQuizzes.length;
    }
``` 

9. W pliku `quiz-list-filter.component.html` dodaj template:
```html
    <div class="QuizListFilter">
      <mat-icon>search</mat-icon>
    
      <input #filter
             type="text"
             placeholder="Filtruj po tytule"
             (input)="onType(filter.value)" />
    
      <div *ngIf="filter.value && resultsAmount"
           class="QuizListFilter__results">
        Wyników: <span class="text-primary">{{resultsAmount}}</span>
      </div>
    </div>
```

10. Dodaj ostylowanie komponentu w pliku `quiz-list-filter.component.scss`:
```css
    @import "../../../../../assets/styles/variables";
    
    .QuizListFilter {
      display: flex;
      position: relative;
      color: $text-dark-color;
      margin-bottom: 25px;
      border-bottom: 1px solid $primary-light-color;
      padding-bottom: 10px;
      line-height: 1.2rem;
    
      &__results {
        line-height: 24px;
      }
    
      .mat-icon {
        color: $primary-light-color;
        margin-right: 16px;
      }
    
      input {
        flex-grow: 1;
        font-size: 1.2rem;
      }
    }
```

11. Przefiltruj quizy (Wymagany przynajmniej 1 quiz w liście) wpisując szukaną frazę w polu wyszukiwania. Sprawdź co się stanie jak usuniesz tą wartość, wpiszesz inną a później wrócisz do poprzedniej.

12. Dodaj dekorator `@Once` do settera `setQuizzes` w pliku `quiz-list-filter.component.ts`:
```javascript
  @Once()
  @Input('quizzes') set setQuizzes(data: IQuiz[]) {
      this.quizzes = data;
  }
```

13. Wykonaj ponownie czynności z pkt. 11

14. Zwróć uwagę jak często podczas aktualizacji wartości filtru wykonywana jest metoda `onType`. 

15. Dodaj dekorator `@Debounce(500)` przed metodą `onType`
```javascript
    // tslint:disable:no-magic-numbers
    @Debounce(300)
    onType(text: string) {
```

16. Dokonaj ponownie obserwacji częstotliwości wykonywania się metody `onType`.

17. Zwróć uwagę jak często wykonywana jest logika funkcji `getFilteredQuizzes` (np. poprzez wstawienie do środka console.log i wykonanie poleceń z punktu 11).
 
18. Dodaj dekorator `@Memoize` do metody `getFilteredQuizzes`.
```javascript
    @Memoize()
    getFilteredQuizzes(text: string): IQuiz[] {
```

19. Dokonaj ponownie obserwacji jak w pkt. 17
