---
title: "*Custom decorator (Bind, Once)"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 30
---

### Instrukcje:

1. Zapoznaj się z dokumentacją dotyczącą dekoratorów:
https://www.typescriptlang.org/docs/handbook/decorators.html

2. Na podstawie dokumentacji dekoratora metody napisz swoją własną implementacje dekoratorów @Bind oraz @Once

3. Podmień wcześniej użyte implementacje dekoratorów lodashowych @Bind i @Once na własne
