---
title: "Lodash decorators"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
chapter: true
pre: "<b>2. </b>"
---

### Rozdział 2

# Lodash decorators

W tym rozdziale nauczysz sie korzystać z biblioteki lodash decorators dodającej nowe dekoratory.

### Git branch:
 
```bash
$ git checkout part2/lodash-decorators-upd
```
