---
title: "Error interceptor - Bind lodash decorator"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
---

### Instrukcje:

1. W pliku `error.interceptor.ts` dodaj przed metodą `handleError$` dekorator `Bind`.
```javascript
    @Bind()
    private handleError$(error: HttpErrorResponse): Observable<never> {
```

2. Edytuj w pliku `error.service.ts` sposób wykorzystania metody `handleError&`.
```javascript
    // wers
    .pipe(catchError(this.handleError$.bind(this)));

    // zamień na
    .pipe(catchError(this.handleError$));
```
