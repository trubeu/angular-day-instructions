---
title: "Quiz Service"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
---

### Instrukcje:

1. W pliku `http.service.ts` dodaj metodę do pobierania danych z API.
```javascript
      get$<T>(url: string): Observable<APIResponse<T>> {
        return this.httpClient
          .get<APIResponse<T>>(`${environment.url}${url}`, {observe: 'body', responseType: 'json'});
      }
```

2. W pliku `quiz.service.ts` dodaj metodę do pobierania listy quizów
```javascript
  fetchQuizzes$(): Observable<IQuiz[]> {
      return this.httpService.get$<IQuiz>('quizzes').pipe(
        map((response: IAPICollectionResponse<IQuiz>) => response.Items),
      );
  }
```

3. W pliku `quiz.service.ts` dodaj metodę do pobierania pojedynczego quizu
```javascript
  fetchQuizById$(id: number): Observable<IQuiz> {
      return this.httpService.get$<IQuiz>(`quizzes/${id}`).pipe(
        map((response: IAPIItemResponse<IQuiz>) => response.Item),
      );
  }
```
