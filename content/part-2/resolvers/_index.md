---
title: "Resolvers"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
chapter: true
pre: "<b>1. </b>"
---

### Rozdział 1

# Resolvers

W tym rozdziale nauczysz sie korzystać z resolverów (metody pobierania danych przed wyświetleniem konkretnej strony).

### Git branch:

```bash
$ git checkout part2/resolvers-upd
```
