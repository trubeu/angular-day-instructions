---
title: "Tworzenie resolvera dla listy quizów"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
---

### Instrukcje: 

1. W katalogu `src/app/core` utwórz katalog `resolvers`, a w nim plik `quizes.resolver.ts`.

2. Resolver to tak naprawdę serwis z metodą `resolve` zwracającą interesujące nas informacje:
```javascript
    @Injectable({
        providedIn: 'root',
    })
    export class QuizesResolver implements Resolve<IQuiz[]> {
        constructor(private quizService: QuizService) {
        }
    
        resolve(): Observable<IQuiz[]> {
            return this.quizService.fetchQuizzes$();
        }
    }
```

3. Przejdź do pliku `quiz-routing.module.ts`. W routingu do ścieżki z komponentem `QuizListComponent` dodaj resolver:
```javascript
    {
        path: RoutesConstants.quiz.LIST,
        component: QuizListComponent,
        resolve: {quizzes: QuizesResolver},
    },
```
