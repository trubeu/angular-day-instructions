---
title: "Quiz List Component"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 30
---

### Instrukcje: 


1. Przejdź do pliku `quiz-list.component.ts` i dodaj w klasie atrybut `quizzes`
```javascript
    quizzes: IQuiz[] = [];
```

2. W tym samym komponencie dodaj także serwis `ActivatedRoute` poprzez dependency injection
```javascript
    constructor(private activatedRoute: ActivatedRoute) {
    }
```

3. W metodzie `ngOnInit` tego komponentu dodaj przypisanie danych z resolvera do atrubytu `quizzes`
```javascript
    this.quizzes = this.activatedRoute.snapshot.data.quizzes;
```

4. Do komponentu dodaj atrybut ze ścieżkami wymaganymi w template
```javascript
    readonly routesConstants = RoutesConstants;
```

5. Zaktualizuj HTML tego komponentu
```html
    <div class="container-content">
      <table *ngIf="quizzes.length; else noQuizzes"
             class="table-text-left">
        <thead>
        <tr>
          <th></th>
          <th>Tytuł</th>
          <th>Opis</th>
          <th>Ilość pytań</th>
          <th>Dodano</th>
          <th></th>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let quiz of quizzes; let i = index" class="x">
          <td># {{ i + 1}}</td>
          <td>{{quiz.title}}</td>
          <td>{{quiz.description}}</td>
          <td>{{quiz.questions.length}}</td>
          <td>{{quiz.date | mdate: {fromNow: true} }}</td>
          <td class="text-right">
            <button mat-flat-button
                    color="primary"
                    class="btn-submit btn-medium"
                    [routerLink]="[routesConstants.quiz.getUrl(routesConstants.quiz.SOLVE), quiz.quizId]">Rozwiąż quiz</button>
          </td>
        </tr>
        </tbody>
      </table>
    
      <ng-template #noQuizzes>
        <div class="no-results">
          <p>
            Do tej pory nie dodano żadnego quizu. Dodaj pierwszy quiz korzystając z
            <a [routerLink]="routesConstants.quiz.getUrl(routesConstants.quiz.ADD)">tego adresu</a>.
          </p>
        </div>
      </ng-template>
    </div>
```
