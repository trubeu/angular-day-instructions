---
title: "Rxjs operator - takeUntil, first, take"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 20
---

### Wstęp

W tej części szkolenia poznasz 3 sposoby na zabijanie subskrypcji obiektów typu observable z rxjs:

* operator first
* operator take
* operator takeUntil

### Instrukcje: 

1. W celach weryfikacji subskrypcji w pliku `quiz-list.component.ts` dodaj metodę `ngOnDestroy`.
Nie zapomnij o rozszerzeniu klasy o interfejs `OnDestroy`.
```javascript
    ngOnDestroy() {
        const clDelay: number = 3000;
    
        setTimeout(() => {
            console.log(this.quizService.getQuizzes$());
        }, clDelay);
    }
```

2. Przejdź do zakładki z listą quizów a następnie przejdź do jakiejkolwiek innej zakładki.

3. Zwróć uwagę na wyświetlony w konsoli wynik wywołania funkcji `this.quizService.getQuizzes` (w szczególności na liczbę subskrypcji `observers`).
    Powtórz krok kilka razy i obserwuj zmiany w wyświetlanym w konsoli obiekcie.
    
4. Edytuj logikę metody `ngOnInit`
```javascript
    ngOnInit() {
        this.quizService
          .getQuizzes$()
          .pipe(take(1))
          .subscribe((quizzes: IQuiz[]) => this.quizzes = quizzes);
    }   
```

5. Powtórz kroki 2-3 i zaobserwuj różnicę.
    
6. Edytuj logikę metody `ngOnInit`
```javascript
    ngOnInit() {
        this.quizService
          .getQuizzes$()
          .pipe(first())
          .subscribe((quizzes: IQuiz[]) => this.quizzes = quizzes);
    }   
```

7. Powtórz kroki 2-3 i zaobserwuj różnicę.

8. Dodaj do klasy atrybut `onDestroy$`
```javascript
    private readonly onDestroy$: Subject<any> = new Subject();
```

9. Edytuj logikę metody `ngOnDestroy`
```javascript
    ngOnDestroy() {
        const clDelay: number = 3000;
        
        this.onDestroy$.next();
        this.onDestroy$.complete();
        
        setTimeout(() => {
            console.log(this.quizService.getQuizzes$());
        }, clDelay);
    }
```
 
10. Edytuj metodę `ngOnInit`
```javascript
    ngOnInit() {
      this.quizService
        .getQuizzes$()
        .pipe(takeUntil(this.onDestroy$))
        .subscribe((quizzes: IQuiz[]) => this.quizzes = quizzes);
    }
```

11. Powtórz punkty 2-3
