---
title: "Rxjs operator - tap"
date: 2019-01-29T19:13:02+01:00
draft: false
weight: 10
---

### Wstęp

W tej części szkolenia użyjesz operatora `tap` z biblioteki rxjs.

### Instrukcje: 

1. Przejdź do pliku `src/app/components/quiz/quiz.service.ts`.

2. Dodaj w serwisie atrybut `quizzes` oraz metody set/get dla tego atrybutu:
```javascript
    private quizzes$: BehaviorSubject<IQuiz[]> = new BehaviorSubject([]);

    getQuizzes$(): BehaviorSubject<IQuiz[]> {
      return this.quizzes$;
    }
    
    setQuizzes(quizzes: IQuiz[]) {
      this.quizzes$.next(quizzes);
    }
```

3. Przejdź do pliku `src/app/core/resolvers/quizzes.resolver.ts`

4. Edytuj logikę metody `resolve`
```javascript
    resolve(): Observable<IQuiz[]> {
        return this.quizService
          .fetchQuizzes$()
          .pipe(
              tap((quizzes: IQuiz[]) => this.quizService.setQuizzes(quizzes)),
          );
    }
```

5. Przejdź do pliku `src/app/components/quiz/quiz-list/quiz-list.component.ts`

6. Edytuj konstruktor tego komponentu
```javascript
    constructor(private quizService: QuizService) {
    }
```

7. Edytuj metodę `ngOnInit`
```javascript
  ngOnInit() {
        this.quizService
          .getQuizzes$()
          .subscribe((quizzes: IQuiz[]) => this.quizzes = quizzes);
  }
```
